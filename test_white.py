import numpy as np
import cv2
import argparse
import glob
import string
import train_white
from sklearn.externals import joblib
import json
import urllib
# import get_parse_data

# from flask import Flask

# app = Flask(__name__)





# Construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument( '--do_prep',  action='store_true', help = "Do you wanna pre-process the images?")
ap.add_argument( '--have_repo',  action='store_true', help = "Do you have a face repo?")
ap.add_argument( '--test',  default='test_images/', help = "Test image repo")
ap.add_argument( '--train',  default='train_images/', help = "Test image repo")
ap.add_argument( '--face_repo',  default='train_images_faces/', help = "Test image repo")


args = vars(ap.parse_args())
print("want preprocessing:", args["do_prep"])
print("Face repo existing:", args["have_repo"])

# @app.route('/')



def test():
	#load the cascades:
	face_cascade_alt_tree = cv2.CascadeClassifier('cascades/haarcascade_frontalface_alt_tree.xml')
	face_cascade_alt = cv2.CascadeClassifier('cascades/haarcascade_frontalface_alt2.xml')
	face_cascade_default = cv2.CascadeClassifier('cascades/haarcascade_frontalface_default.xml')

	test_images = [cv2.imread(file) for file in glob.glob(args["test"]+'*.jpg')]
	names = train_white.load_names()
	images_uid = train_white.load_uid()
	# model = load_model('lbp.model')
	recognizer = train_white.train(args["do_prep"], args["have_repo"])
	print("started testing")
	for image in test_images:
		gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
		gray = np.array(gray, 'uint8')
		#detect faces:
		faces = [None]
		faces = face_cascade_alt.detectMultiScale(gray, 1.3, 5)
		if not (faces.__len__()==0):
			[x, y, w, h] = faces[0]
			if not (int(w)<90 or int(h)<90):
				label_predicted, conf = recognizer.predict(cv2.resize(gray[y: y + h, x: x + w], (500,500), interpolation=cv2.INTER_AREA))
				cv2.imshow("Recognizing Face", image[y: y + h, x: x + w])
				cv2.waitKey(0)
				n = names[label_predicted]
				p = images_uid[label_predicted]
				post = {"teamname":"white wolves","imageuid":p, "name":n}
				# response = requests.post(url, data=json.dumps(post), headers=headers)
				print(post)
				# @app.route('/')
				# def display():
				# 	return ("Face_ID :", names[label_predicted])
		else:
			faces = [None]
			faces = face_cascade_default.detectMultiScale(gray, 1.2, 5)
			if not (faces.__len__()==0):
				[x, y, w, h] = faces[0]
				if not (int(w)<90 or int(h)<90):
					label_predicted, conf = recognizer.predict(cv2.resize(gray[y: y + h, x: x + w], (500,500), interpolation=cv2.INTER_AREA))
					cv2.imshow("Recognizing Face", image[y: y + h, x: x + w])
					cv2.waitKey(0)
					n = names[label_predicted]
					p = images_uid[label_predicted]
					post = {"teamname":"white wolves","imageuid":p, "name":n}
					# response = requests.post(url, data=json.dumps(post), headers=headers)
					print(post)
			else:
				post = {"teamname":"white wolves","imageuid":None, "name":None}
				# response = requests.post(url, data=json.dumps(post), headers=headers)	

				# @app.route('/')
				# def display():
				# 	return ("Face_ID :", names[label_predicted])


if __name__ == '__main__':
	# app.run()
	url = 'http://us-central1-lynkhacksmock.cloudfunctions.net/verifyfaces'
	headers = {'str':'str', 'str':'int', 'str':'str'}
	# req = urllib2.Request('http://us-central1-lynkhacksmock.cloudfunctions.net/verifyface')
	test()




