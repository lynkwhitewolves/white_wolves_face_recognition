import csv
import string
import requests
import json
from pprint import pprint



def parse_uid(X):
	images_uid = []
	for i in range(len(X)):
		images_uid.append(X[i]['imageuid'])
	return images_uid


def parse_url(X):
	images_url = []
	for i in range(len(X)):
		images_url.append(X[i]['url'])
	return images_url


def parse_names(X, label=False):
	label_names = []
	for i in range(len(X)):
		label_names.append(X[i]['name'])
	# label_names = (X[2])
	if label == True:
		return label_names
	names = list(set(label_names))
	return names




def download_images(X):
	label_names = parse_names(X, label = True)
	images_url = parse_url(X)
	for i in range(len(X)):
		print(i)
		url = images_url[i]
		r = requests.get(url, allow_redirects=True)
		filename = "temp/"+label_names[i]+"_"+str(i)+".jpg"
		open(filename, 'wb').write(r.content)



if __name__ == '__main__':	
	train_url = 'https://us-central1-lynkhacksmock.cloudfunctions.net/faceimages'
	r = requests.get(train_url, allow_redirects=True)
	filename = "data.json"
	open(filename, 'wb').write(r.content)
	X = []
	with open('data.json') as data_file:    
	    X = json.load(data_file)
	print(X[0]['imageuid'])
	# X = []
	download_images(X)






