import numpy as np
import cv2
import argparse
import glob
import csv
import string
import requests
import random
import get_parse_data
from sklearn.externals import joblib
import json

# def save_model(filename, model):
#     joblib.dump(model, filename, compress=9)
    
# def load_model(filename):
#     return joblib.load(filename)

random.seed()

# ap = argparse.ArgumentParser()

# args = vars(ap.parse_args())

def load_names():
	# train_url = 'https://us-central1-lynkhacksmock.cloudfunctions.net/faceimages'
	# r = requests.get(train_url, allow_redirects=True)
	# filename = "data.json"
	# open(filename, 'wb').write(r.content)
	with open('data.json') as data_file:
		X = json.load(data_file)
		print(X[0])
	return get_parse_data.parse_names(X)



def load_uid():
	# train_url = 'https://us-central1-lynkhacksmock.cloudfunctions.net/faceimages'
	# r = requests.get(train_url, allow_redirects=True)
	# filename = "data.json"
	# open(filename, 'wb').write(r.content)

	with open('data.json') as data_file:    
	    X = json.load(data_file)
	return get_parse_data.parse_uid(X)

def preprocess():
	for file in glob.glob(args["train"]+'*.jpg'):
		prep_image = cv2.imread(file)

		image_hsv = cv2.cvtColor(prep_image, cv2.COLOR_BGR2HSV)
		cv2.imwrite(args["train"]+file.split("/")[1].split("_")[0]+"_"+str(random.randint(111,9999))+".jpg", image_hsv)

		image_lab = cv2.cvtColor(prep_image, cv2.COLOR_BGR2LAB)
		cv2.imwrite(args["train"]+file.split("/")[1].split("_")[0]+"_"+str(random.randint(111,9999))+".jpg", image_lab)
		
		image_flipped = cv2.flip(prep_image,1)
		cv2.imwrite(args["train"]+file.split("/")[1].split("_")[0]+"_"+str(random.randint(111,9999))+".jpg", image_flipped)
		
		# image_gaussian = cv2.GaussianBlur(prep_image,(5,5),0)
		# cv2.imwrite("args["train"]/"+file.split("/")[1].split("_")[0]+"_"+str(random.randint(111,9999))+".jpg", image_gaussian)

		# _, image_threhold = cv2.threshold(prep_image, 127,200, cv2.THRESH_BINARY_INV)
		# cv2.imwrite("args["train"]/"+file.split("/")[1].split("_")[0]+"_"+str(random.randint(111,9999))+".jpg", image_threhold)		

		# image_laplacian = cv2.Laplacian(prep_image, cv2.CV_64F)
		# cv2.imwrite("args["train"]/"+file.split("/")[1].split("_")[0]+"_"+str(random.randint(111,9999))+".jpg", image_threhold)

		# image_hsv = cv2.cvtColor(prep_image, cv2.COLOR_BGR2HSV)
		# cv2.imwrite(file.split("/")[1].split("_")[0]+"_"+random.randint(111,999)+".jpg")
	return 


def train(do_prep, have_repo):
	print("Do we have a repo of faces:", have_repo)
	names = load_names()
	print("Loaded names into the database")
	if do_prep:
		preprocess()
		print("Done pre-processing of training images")
	# recognizer = cv2.face.createLBPHFaceRecognizer()
	# recognizer = cv2.face.createFisherFaceRecognizer()
	recognizer = cv2.face.createEigenFaceRecognizer()
	
	face_cascade = cv2.CascadeClassifier('cascades/haarcascade_frontalface_default.xml')
	face_cascade_alt = cv2.CascadeClassifier('cascades/haarcascade_frontalface_alt2.xml')

	i = 0
	labels = []
	t_images = []
	print("staring the training")
	
	#when you don't have a trained repo of faces:
	if (have_repo == False):
		print("first create a face repo of trainable images")
		print("counting faces")
		for file in glob.glob(args["train"]+'*.jpg'):
			# print(file)
			image = cv2.imread(file)
			gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
			gray = np.array(gray, 'uint8')
			#detect faces:
			faces = face_cascade.detectMultiScale(gray, 1.5, 10)
			if not (faces.__len__()==0):
				i+=1
				print(i)
				[x, y, w, h] = faces[0]
				face_roi = gray[y: y + h, x: x + w]
				t_images.append(face_roi)
				name = file.split("/")[1].split("_")[0]
				label = names.index(name)
				labels.append(label)
				cv2.imwrite(args["face_repo"]+str(file.split("/")[1]), face_roi)
			else:
				faces = face_cascade_alt.detectMultiScale(gray, 1.3, 5)
				if not(faces.__len__()==0):
					i+=1
					print(i)
					[x, y, w, h] = faces[0]
					face_roi = gray[y: y + h, x: x + w]
					t_images.append(face_roi)
					name = file.split("/")[1].split("_")[0]
					label = names.index(name)
					labels.append(label)
					cv2.imwrite(args["face_repo"]+str(file.split("/")[1]), face_roi)


	#when you have an already ready repo of faces:
	else:
		print("i'm here")
		for file in glob.glob('train_images_faces/*.jpg'):
			image = cv2.imread(file)
			image = cv2.resize(cv2.imread(file), (500,500), interpolation = cv2.INTER_AREA)
			image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
			t_images.append(np.array(image, 'uint8'))
			label = names.index(file.split("/")[1].split("_")[0])
			labels.append(label)

	#Finally! Train the model:
	recognizer.train(t_images, np.array(labels))
	# save_model('lbp.model', model)
	return recognizer


if __name__ == '__main__':
	train(False, True)