import numpy as np
import cv2
import argparse
import glob
import csv
import string
import requests

# with open('train_images/ts.csv') as f:
#     reader = csv.reader(f, delimiter=',')
#     X = [[str(col1), str(col2), str(col3)]
#                 for col1, col2, col3 in reader]
#     X = [[j[i] for j in X] for i in range(len(X[0]))]


# for i in range(len(X[1])):
# 	X[1][i] = str(X[1][i])
# 	X[1][i] = X[1][i][5:-1]
# 	images_url = (X[1])


# for i in range(len(X[2])):
# 	X[2][i] = str(X[2][i])
# 	X[2][i] = X[2][i][6:-1]
# 	names = (X[2])

# names = list(set(names))
# print(names)

names = ['harish']


face_cascade = cv2.CascadeClassifier('cascades/haarcascade_frontalface_default.xml')
print(face_cascade)
person_cascade = []
for index, name in enumerate(names):
	# print(index, name)
	cascade_name_and_path = "cascades/"+str(name)+".xml"
	p = cv2.CascadeClassifier(cascade_name_and_path)
	# print(p)
	person_cascade.append(p)


print(person_cascade)
# construct the argument parse and parse the arguments
# ap = argparse.ArgumentParser()
# ap.add_argument("-f", "--folder", required=True,
#     help="path to input image folder")
# args = vars(ap.parse_args())



test_images = [cv2.imread(file) for file in glob.glob('test_images/*.jpg')]

for index, image in enumerate(test_images):
	print("===================== Looping through the", index, "image in test set ===========================")
	gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
	#detect faces:
	faces = face_cascade.detectMultiScale(gray, 1.2, 5)

	if not (faces.__len__()==0):
	    print("found face")
	else:
	    print("no face")
	for index, (x,y,w,h) in enumerate(faces):
		cv2.rectangle(gray,(x,y), (x+w,y+h), (255,0,0),2)
		cv2.rectangle(image,(x,y), (x+w,y+h), (255,0,0),2)
		roi_gray = gray[y:y+h, x:x+w]
		roi_color = image[y:y+h, x:x+w]
		cv2.imshow("roi_image",image)
		cv2.waitKey(0)
	
	#person specific cascades:
	detected = False
	for i in range(len(names)):
		person = [None]
		person = person_cascade[i].detectMultiScale(roi_gray, 1.05, 5)
		if not (person.__len__()==0):
			detected = True
			print("Valid ID; Detected person in the image:", names[i])
			# for (x,y,w,h) in person:
			# 	cv2.rectangle(roi_gray,(x,y),(x+w,y+h),(255,0,0),2)
			# 	face_box = roi_gray[y:y+h, x:x+w]
			# 	cv2.imshow('face',roi_gray)
			# 	cv2.waitKey(0)
			break
	if detected == True:
		continue
	else:
		print("No record in database; Invalid id")
	cv2.waitKey(0)

 

# print(len(person))
# print(person)
# print(detected)
